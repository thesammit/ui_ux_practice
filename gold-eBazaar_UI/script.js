document.addEventListener('DOMContentLoaded', (event) => {
    let hamburgerMenu = document.querySelector('.hamburger');
    hamburgerMenu.addEventListener('click', toggleSideNav, false);

    let closeSideBar = document.querySelector('.close-btn');
    closeSideBar.addEventListener('click', toggleSideNav, false);

    let profileDrop = document.querySelector('.user-details .user-action');
    profileDrop.addEventListener('click', toggleProfileDropDown, false);
})

let toggleSideNav = () => {
    let sideNav = document.querySelector('#side-nav-bar');
    sideNav.classList.toggle('open');
}

let toggleProfileDropDown = (event) => {
    let profileDrop = event.target;
    if (profileDrop.classList.contains('fa-chevron-down')) {
        profileDrop.classList.replace('fa-chevron-down', 'fa-chevron-up')
    } else {
        profileDrop.classList.replace('fa-chevron-up', 'fa-chevron-down')
    }
    let profileDropDown = document.querySelector('#user-profile-dropdown');
    profileDropDown.classList.toggle('hide-element');
}

window.onclick = (event) => {
    let profileDropDown = document.querySelector('#user-profile-dropdown');
    let profileDrop = document.querySelector('#toggle-profile-action');
    let target = event.target.closest('#user-profile-dropdown');
    if ((!target && !event.target.matches('#toggle-profile-action')) &&
        !profileDropDown.classList.contains('hide-element')) {
        profileDropDown.classList.add('hide-element');
        profileDrop.classList.replace('fa-chevron-up', 'fa-chevron-down')
    }

}