document.addEventListener('DOMContentLoaded', (event) => {
    let allTiles = document.querySelectorAll('.tile');
    allTiles.forEach(element => {
        element.addEventListener('click', handleClickOnTile, false);
    });
})

let handleClickOnTile = (event) => {
    event.target.classList.toggle('tile-back');
}